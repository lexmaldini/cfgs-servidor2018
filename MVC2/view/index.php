<?php

require_once(__DIR__.'/../controller/DateResultsController.php');

$cnt = new DateResultsController();
$jornadas = $cnt->getMoreDateResults(38);

?>
<html>
    <head>
        <title>Foo</title>
    </head>
    <body>
        <div id="wrapper">
            <?php foreach($jornadas as $jornada) { ?>
            <h1><?=$jornada->num?></h1>
            <table>
                <tr><th>Local</th><th>Visitante</th><th>Resultado</th></tr>
                <?php foreach($jornada->results as $res){ ?>
                <tr>
                    <td><?=$res->home->name?></td>
                    <td><?=$res->away->name?></td>
                    <td><?=$res->code?></td>
                </tr>
                <?php } ?>
            </table>
            <?php } ?>
        </div>
    </body>
</html>