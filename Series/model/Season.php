<?php 

class Season{
    
    private $_idseason;
    private $_year;
    private $_nchapters;
    private $_season;
    private $_serie;
    
    public function __construct($y, $nch, $s, $idseason, $serie){
        $this->setYear($y);
        $this->setNchapters($nch);
        $this->setSeason($s);
        $this->setIdseason($idseason);
        $this->setSerie($serie);
    }
    
    public function getIdseason()
    {
        return $this->_idseason;
    }

    public function getYear()
    {
        return $this->_year;
    }

    public function getNchapters()
    {
        return $this->_nchapters;
    }

    public function getSeason()
    {
        return $this->_season;
    }

    public function getSerie()
    {
        return $this->_serie;
    }

    public function setIdseason($_idseason)
    {
        $this->_idseason = $_idseason;
    }

    public function setYear($_year)
    {
        $this->_year = $_year;
    }

    public function setNchapters($_nchapters)
    {
        $this->_nchapters = $_nchapters;
    }

    public function setSeason($_season)
    {
        $this->_season = $_season;
    }

    public function setSerie($_serie)
    {
        $this->_serie = $_serie;
    }

    public function toArray(){
        $vars = get_object_vars ( $this );
            $array = array ();
            foreach ( $vars as $key => $value ) {
                $array [ltrim ( $key )] = $value;
            }
            return $array;
      }
    
    
}