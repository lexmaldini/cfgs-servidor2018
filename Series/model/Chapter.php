<?php

class Chapter{
    
    private $_idchapter;
    private $_title;
    private $_director;
    private $_writter;
    private $_adate;
    private $_season;
    
    public function __construct($t, $d, $w, $ad, $idchapter, $season){
        $this->setTitle($t);
        $this->setDirector($d);
        $this->setWritter($w);
        $this->setAdate($ad);
        $this->setIdchapter($idchapter);
        $this->setSeason($season);
    }
    
    public function getIdchapter()
    {
        return $this->_idchapter;
    }

    public function getTitle()
    {
        return $this->_title;
    }

    public function getDirector()
    {
        return $this->_director;
    }

    public function getWritter()
    {
        return $this->_writter;
    }

    public function getAdate()
    {
        return $this->_adate;
    }

    public function getSeason()
    {
        return $this->_season;
    }

    public function setIdchapter($_idchapter)
    {
        $this->_idchapter = $_idchapter;
    }

    public function setTitle($_title)
    {
        $this->_title = $_title;
    }

    public function setDirector($_director)
    {
        $this->_director = $_director;
    }

    public function setWritter($_writter)
    {
        $this->_writter = $_writter;
    }

    public function setAdate($_adate)
    {
        $this->_adate = $_adate;
    }

    public function setSeason($_season)
    {
        $this->_season = $_season;
    }

    
    
    
    
}