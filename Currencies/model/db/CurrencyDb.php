<?php

require_once(__DIR__.'/../Currency.php');

class CurrencyDb{
    
    private $conn;
    
    public function getCurrencies(){
        $this->openConnection();
        
        $sql = "SELECT * FROM currency";
        $stm = $this->conn->prepare($sql);
        
        $stm->execute();
        $result = $stm->get_result();
        
        $ret = array();
        while($r = $result->fetch_assoc()){
            $curr = new Currency($r['name'], $r['symbol'], 
                    $r['eurval'], $r['type']);
            array_push($ret, $curr);
        }
        return $ret;
        
    }
    
    public function getCurrency($s){
        $this->openConnection();
        
        $sql = "SELECT * FROM currency WHERE symbol = ? ";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("s", $ssql);
        $ssql = $s;
        
        $stm->execute();
        $result = $stm->get_result();
        
        $r = $result->fetch_assoc();
        $curr = new Currency($r['name'], $r['symbol'],
                $r['eurval'], $r['type']);
        return $curr;
        
    }
    
    public function createCurrency($cn, $cs, $cev, $ct){
        $this->openConnection();
        
        $sql = "INSERT INTO currency ( name, symbol, type, eurval ) VALUES (?, ?, ?, ?)";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("ssss", $n, $s, $t, $ev);
        $n = $cn;
        $s = $cs;
        $t = $ct;
        $ev = $cev;
        
        $stm->execute();
        $result = $stm->get_result();
        
        return new Currency($cn, $cs, $cev, $ct);
        
    }
    
    public function updateCurrency($co, $cn, $cs, $cev, $ct){
        $this->openConnection();
        
        $sql = "UPDATE currency SET name = ? , symbol = ? , type = ? , eurval = ? WHERE symbol = ? ";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("sssss", $n, $s, $t, $ev, $o);
        $n = $cn;
        $s = $cs;
        $t = $ct;
        $ev = $cev;
        $o = $co;
        
        $stm->execute();
        $result = $stm->get_result();
        
        return $this->getCurrency($cs);
        
    }
    
    private function openConnection(){
        if($this->conn == null){
            $this->conn = mysqli_connect("127.0.0.1",
                "money",
                "Money!123",
                "money");
        }
    }
    
}