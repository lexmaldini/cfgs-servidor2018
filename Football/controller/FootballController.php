<?php

require_once(__DIR__.'/../model/db/TeamDb.php');

class FootballController{
    
    public function getTeam($id){
        $db = new TeamDb();
        $team = $db->findOneTeam($id);
        return $team;
    }

    public function getTeams(){
        $db = new TeamDb();
        $teams = $db->findAllTeams();
        return $teams;
    }
    
    public function saveTeam($team){
        $db = new TeamDb();
        $id = $db->insertTeam($team);
        return $id;
    }

    public function deleteTeam($id){
        $db = new TeamDb();
        $team = $db->deleteTeam($id);
        return $team;
    }

    public function updateTeam($team){
        $db = new TeamDb();
        $team = $db->updateTeam($team);
        return $team;
    }
    
    
    
}