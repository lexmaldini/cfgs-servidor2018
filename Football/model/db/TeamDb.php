<?php 

require_once(__DIR__.'/../Team.php');

class TeamDb{

    public function findOneTeam($id){
        $collection = (new MongoDB\Client)->football->team;
        $cursor = $collection->findOne([
            '_id' => new MongoDB\BSON\ObjectId($id),
        ]);

        $stname = $cursor['stadium']['name'];
        $stloc = array();
        $stloc[0] = $cursor['stadium']['loc']['long'];
        $stloc[1] = $cursor['stadium']['loc']['lat'];
        $stadium = new Stadium($stname, $stloc);
        $teid = $cursor['_id'];
        $tename = $cursor['name'];
        $tefound = $cursor['founded'];
            
        $team = new Team($tename, $tefound, $stadium, $teid);

        return $team;
    }
    
    public function findAllTeams(){
        $collection = (new MongoDB\Client)->football->team;
        $cursor = $collection->find([]);
        
        $ret = array();
        foreach ($cursor as $mongoteam){
            $stname = $mongoteam['stadium']['name'];
            $stloc = array();
            $stloc[0] = $mongoteam['stadium']['loc']['long'];
            $stloc[1] = $mongoteam['stadium']['loc']['lat'];
            
            $stadium = new Stadium($stname, $stloc);
            
            $teid = $mongoteam['_id'];
            $tename = $mongoteam['name'];
            $tefound = $mongoteam['founded'];
            
            $team = new Team($tename, $tefound, $stadium, $teid);
            array_push($ret, $team);
        }
        return $ret;
    }
    
    /**
     * Param should be a Team class instance.
     * 
     * @param Team $team
     */
    public function insertTeam($team){
        $collection = (new MongoDB\Client)->football->team;
        
        $insertOneResult = $collection->insertOne($team->toArray());
        $id = $insertOneResult->getInsertedId();
        return $id;
    }

    public function updateTeam($team){
        $collection = (new MongoDB\Client)->football->team;

        $stname = $team->getStadium()->getName();
        $stloc = array();
        $stloc[0] = $team->getStadium()->getLoc()[0];
        $stloc[1] = $team->getStadium()->getLoc()[1];
            
        $teid = $team->getTid();
        $tename = $team->getName();
        $tefound = $team->getFounded();

        $updateOneResult = $collection->updateOne(
            ['_id' =>  new MongoDB\BSON\ObjectId($teid)],
            ['$set'=> ['name'=> $tename,
            'founded'=>$tefound,
            'stadium'=>['name'=>$stname,'loc'=>['long'=>$stloc[0],'lat'=>$stloc[1]]],
            ]
        ]);
        
        return $team;
    }

    public function deleteTeam($id){
        $collection = (new MongoDB\Client)->football->team;
        $team = $this->findOneTeam($id);
        
        $deleteOneResult = $collection->deleteOne([
            '_id' => new MongoDB\BSON\ObjectId($id),
        ]);
        
        return $team;
    }
    
}