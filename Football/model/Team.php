<?php 

require_once(__DIR__.'/Stadium.php');

class Team{

    private $_tid;
    private $_name;
    private $_founded;
    private $_stadium;
    
    public function __construct($n, $f, $s, $id=null){
        $this->setName($n);
        $this->setFounded($f);
        $this->setStadium($s);
        $this->setTid($id);
    }

    public function getTid()
    {
        return $this->_tid;
    }
    
    public function getName()
    {
        return $this->_name;
    }

    public function getFounded()
    {
        return $this->_founded;
    }

    public function getStadium()
    {
        return $this->_stadium;
    }

    public function setTid($_tid)
    {
        $this->_tid = $_tid;
    }

    public function setName($_name)
    {
        $this->_name = $_name;
    }

    public function setFounded($_founded)
    {
        $this->_founded = $_founded;
    }

    /**
     * This sould be a Stadium class instance
     * 
     * @param Stadium $_stadium
     */
    public function setStadium($_stadium)
    {
        $this->_stadium = $_stadium;
    }
    
    public function toArray(){
        $arr = array();
        $arr['name'] = $this->getName();
        $arr['founded'] = $this->getFounded();
        $arr['stadium'] = $this->getStadium()->toArray();
        return $arr;
    }
    
    public function tojson(){
        $arr = $this->toArray();
        return json_encode($arr);
    }

    
}